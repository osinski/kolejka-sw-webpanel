#
# cancomm.py
# Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#
import logging
from threading import Lock

from socketcan import CanFrame

import canbus
import msg_handler

""" List of last <numberOfFramesToDisplay> CAN frames . """
framesToDisplay: list[tuple[str, CanFrame]] = []
""" Lock to synchronize updating of <framesToDisplay> list. """
lockUpdatingFramesToDisplay = Lock()


def busInit(interfaceName: str = "can0"):
    try:
        canbus.socket = canbus.CanRawSocket(interface=interfaceName,
                                                  use_can_fd=False)
        canbus.isActive = True
        logging.info(
            "Inicjalizacja warstwy sprzętowej magistrali CAN"
        )
    except:
        canbus.isActive = False
        logging.warn(
            "Nie można zainicjalizować warstwy sprzętowej magistrali CAN"
        )


def isBusAvailable() -> bool:
    return canbus.isActive


def scheduleToSend(id, data):
    """ Put CAN frame id and data into CAN frame Tx Queue. """
    canbus.canTxFramesQueue.put(
        CanFrame(can_id=id, data=bytes(data))
    )

    lockUpdatingFramesToDisplay.acquire()
    framesToDisplay.append(('<-', CanFrame(can_id=id, data=bytes(data))))
    if len(framesToDisplay) > canbus._NO_OF_FRAMES:
        framesToDisplay.pop(0)
    lockUpdatingFramesToDisplay.release()


def handleReceived():
    """ Thread for handling received data. """
    logging.info("Start wątku obsługi przychodzących danych")
    while True:
        frameToHandle = canbus.canRxFramesQueue.get()

        lockUpdatingFramesToDisplay.acquire()
        framesToDisplay.append(('->', frameToHandle))
        if len(framesToDisplay) > canbus._NO_OF_FRAMES:
            framesToDisplay.pop(0)
        lockUpdatingFramesToDisplay.release()

        msg_handler.dispatchFrame(frameToHandle)


def trafficToString() -> str:
    """
    Generate string representing current traffic on CAN Bus to be displayed
    one webpanel.
    """
    response: str = ""

    if lockUpdatingFramesToDisplay.locked() is not True:
        for frame in framesToDisplay:
            response += frame[0]
            response += ","
            response += str(hex(frame[1].can_id))
            response += ","
            if len(frame[1].data) > 0:
                # Display data in reverse order so MSB is put first
                for data in reversed(frame[1].data):
                    response += f"0x{data:02x}"
                    response += " - "
                response = response[:-3]
            else:
                response += ""
            response += ","

    return response[:-1]
