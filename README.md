# Hardware

This piece of software is meant to be run on RPi (tested on 3B) with CAN Bus
capabilities, i.e. equipped with 
[this shield](https://gitlab.com/osinski/kolejka-hw-rpi-can-shield/).

Some configuration is required (works on kernel v`5.15`):

* make sure system (*raspbian*) is up to date:
    - `sudo apt-get update`
    - `sudo apt-get upgrade`
* edit */boot/config.txt* to enable **SPI** and **MCP2515** overlay; add:
    - `dtparam=spi=on`
    - `dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=5`
    - `dtoverlay=spi0-hw-cs`
* install `can-utils`:
    - `sudo apt-get install can-utils`
* set up CAN interface:
    - `sudo ip link set can0 up type can bitrate 125000`
    - use `ifconfig` to confirm it's up and running
* connect to CAN bus and test:
    - `candump can0` to print traffic on the bus
    - `cansend can0 <frame-id>#<frame-data>` to send a frame
* you can use `vcan` for testing:
    - `sudo ip link add type vcan`
    - `sudo ip link set vcan0 up`


# Prerequisites

* Python 3 (written and tested with Python 3.9/3.10)
* Flask (`python -m pip install flask`)
* Dataclasses-JSON (`python -m pip install dataclasses-json`)
* socketcan (`python -m pip install socketcan`)

*You can use `python -m pip -r ./requirements.txt` to install them in one go.*

# How to run

* Use `run.sh` helper script.
