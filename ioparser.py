#
# parser.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import logging


def logicOR(args: list[bool]) -> bool:
    """ Function performing logic OR on input args. """
    return any(args)


def logicAND(args: list[bool]) -> bool:
    """ Function performing logic AND on input args. """
    return all(args)


""" Dict holding both logic functions. """
logicFunctions = {
    '|': logicOR,
    '&': logicAND
}


class ParseException(Exception):
    """ Class representing Exception that could be thrown during parsing. """
    pass


def _parenthesize(string: str) -> str:
    """
    Fully parenthesize the expression, inserting a number of parentheses around
    each operator, such that they lead to the correct precedence even when
    parsed with a linear, left-to-right parser.
    (en.wikipedia.org/wiki/Operator-precedence_parser)
    """

    outputStr = "(((("

    for char in string:
        if char == '(':
            outputStr += "(((("
        elif char == ')':
            outputStr += "))))"
        elif char == '&':
            outputStr += "))&(("
        elif char == '|':
            outputStr += ")))|((("
        elif char != ' ':
            outputStr += char

    outputStr += "))))"

    return outputStr


def _tokenize(string: str) -> list[str]:
    """
    Extract tokens (operands and their arguments) from given string
    """
    bracketStack: list[int] = []
    bracesStack: list[int] = []
    tokens: list[str] = []

    for i, char in enumerate(string):
        if char == '(':
            bracketStack.append(i)
            tokens.append(char)

        elif char == ')':
            if bracketStack:
                bracketStack.pop()
                tokens.append(char)
            else:
                raise ParseException("')' bez poprzedzającego '('.")

        elif char == '{':
            if not bracesStack:
                bracesStack.append(i)
            else:
                raise ParseException("Zagnieżdzone {")

        elif char == '}':
            if bracesStack:
                openingBracePos = bracesStack.pop()
                try:
                    name, state = string[openingBracePos+1:i].split(',')
                except ValueError as e:
                    raise ParseException("Więcej niż 1 ',' wewnątrz klamr") from e

                if int(state.strip()) == 0:
                    name = '!' + name

                tokens.append(name)
            else:
                raise ParseException("} bez poprzedzającego {.")

        elif char == '&' or char == '|':
            tokens.append(char)

    return tokens


def _infixToPostfix(tokens: list[str]) -> list[str]:
    """
    Use Shunting Yard algorithm to reorganize already tokenized parser input
    string from infix notation to postfix (reverse polish notation) to ease
    handling
    """

    opsStack: list[str] = []
    iosList: list[str] = []

    for token in tokens:
        if token == '&' or token == '|' or token == '(':
            while opsStack and token == '|' and opsStack[-1] == '&':
                iosList.append(opsStack.pop())
            opsStack.append(token)
        elif token == ')':
            while opsStack[-1] != '(':
                iosList.append(opsStack.pop())
            # and now delete the '(' as well
            opsStack.pop()
        else:
            iosList.append(token)

    while opsStack:
        iosList.append(opsStack.pop())

    return iosList


def _evaluateRPN(rpnTokens: list[str]) -> list[dict]:
    """
    Function to evaluate previously obtained Reverse Polish Notation of
    string to parse. Returns final, parsed, form of input string.
    """
    tempStack: list = []
    parsedOutput: list[dict] = []

    for token in rpnTokens:
        if token == '&' or token == '|':
            arg2 = tempStack.pop()
            arg1 = tempStack.pop()
            tempStack.insert(-2, '=')
            parsedOutput.append({
                "op": token,
                "args": [arg1, arg2]
            })
        else:
            tempStack.append(token)

    return parsedOutput


def parse(stringToParse: str) -> list[dict]:
    """
    Function to parse strings with conditions to enable/disable and turn them
    into some other representation that's easier to handle in the runtime.
    """
    parsedOps: list[dict] = []

    if stringToParse == '' or stringToParse is None:
        raise ParseException('String do parsowania jest pusty!')

    logging.debug(f"Parser: String: {stringToParse}")

    tokens = _tokenize(stringToParse)
    logging.debug(f"Parser: tokens: {tokens}")

    if len(tokens) == 1:
        parsedOps.append({
            "op": '&',
            "args": [tokens[0], tokens[0]]
        })
    else:
        tokens = _infixToPostfix(tokens)
        logging.debug(f"Parser: RPN tokens: {tokens}")
        parsedOps = _evaluateRPN(tokens)

    logging.debug(f"Parser: parsedOps: {parsedOps}")

    return parsedOps
