import unittest

import logging
import msg_handler
from socketcan import CanFrame
from cannodes import nodesStatuses, generateListOfNodes
from ios import loadIOsFromJSON, inputsOutputs


class Testmsghandler(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.INFO,
                            format='%(levelname)s @ %(asctime)s: %(message)s',
                            datefmt='%H:%M:%S')
        loadIOsFromJSON()
        generateListOfNodes(inputsOutputs)

    def testmsgparser(self):
        testId = 0x100
        testData = [0x50, 0x40, 0x30, 0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x301
        testData = [0x50, 0x40, 0x30, 0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x311
        testData = [0x50, 0x40, 0x30, 0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x321
        testData = [0x50, 0x40, 0x30, 0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x301
        testData = [0x60, 0x50, 0x40, 0x30, 0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x312
        testData = [0x20, 0x10, 0x00]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        print("")
        testId = 0x001
        testData = [0x03, 0x02, 0x01]
        logging.info(f"Testowanie ramki z ID {hex(testId)}")
        testFrame = CanFrame(can_id=testId, data=bytearray(testData))
        msg_handler.dispatchFrame(testFrame)

        logging.debug(f"NODES STATUSES:\r\n{nodesStatuses}")
