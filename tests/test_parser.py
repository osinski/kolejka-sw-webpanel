import unittest

import logging
import ioparser


class Testioparser(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(levelname)s @ %(asctime)s: %(message)s',
                            datefmt='%H:%M:%S')

    def testioparser(self):
        testInput = 'I{testInput, 0}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['!testInput', '!testInput']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} & O{testOutput1, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['!testInput', 'testOutput1']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} | O{testOutput1, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '|',
            "args": ['!testInput', 'testOutput1']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} & O{testOutput1, 1} & I{testInput2, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['testOutput1', 'testInput2']
        }, {
            "op": '&',
            "args": ['=', '!testInput']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} | O{testOutput1, 1} | I{testInput2, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '|',
            "args": ['testOutput1', 'testInput2']
        }, {
            "op": '|',
            "args": ['=', '!testInput']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} & (O{testOutput1, 1} | O{testOutput2, 0})'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '|',
            "args": ['testOutput1', '!testOutput2']
        }, {
            "op": '&',
            "args": ['=', '!testInput']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} | O{testOutput1, 1} & O{testOutput2, 0}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['testOutput1', '!testOutput2']
        }, {
            "op": '|',
            "args": ['=', '!testInput']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} & O{testOutput1, 1} | O{testOutput2, 0}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['!testInput', 'testOutput1']
        }, {
            "op": '|',
            "args": ['=', '!testOutput2']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} & O{testOutput1, 1} & ' + \
            'O{testOutput2, 0} | O{testOutput3, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['testOutput1', '!testOutput2']
        }, {
            "op": '&',
            "args": ['=', '!testInput']
        }, {
            "op": '|',
            "args": ['=', 'testOutput3']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = 'I{testInput, 0} | O{testOutput1, 1} | ' + \
            'O{testOutput2, 0} & O{testOutput3, 1}'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '&',
            "args": ['!testOutput2', 'testOutput3']
        }, {
            "op": '|',
            "args": ['!testInput', 'testOutput1']
        }, {
            "op": '|',
            "args": ['=', '=']
        }]
        self.assertEqual(str(output), str(expectedOutput))

        print('\n')
        testInput = \
            '(I{testInput, 0} & (O{testOutput1, 1} | O{testOutput4, 1})) | ' \
            + '(O{testOutput2, 0} & O{testOutput3, 1})'
        output = ioparser.parse(testInput)
        expectedOutput = [{
            "op": '|',
            "args": ['testOutput1', 'testOutput4']
        }, {
            "op": '&',
            "args": ['=', '!testInput']
        }, {
            "op": '&',
            "args": ['!testOutput2', 'testOutput3']
        }, {
            "op": '|',
            "args": ['=', '=']
        }]
        self.assertEqual(str(output), str(expectedOutput))


if __name__ == '__main__':
    unittest.main()
