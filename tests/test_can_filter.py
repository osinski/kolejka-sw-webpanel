import unittest

import logging
import canbus
from socketcan import CanFrame


class Testcanfilter(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(levelname)s @ %(asctime)s: %(message)s',
                            datefmt='%H:%M:%S')

    def testcanfilter(self):
        testAddr = 0x001
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x007
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x301
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x307
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x341
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x367
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x381
        self.assertTrue(canbus.acceptMsg(testAddr))
        testAddr = 0x387
        self.assertTrue(canbus.acceptMsg(testAddr))

        testAddr = 0x000
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x009
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x100
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x200
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x300
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x340
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x390
        self.assertFalse(canbus.acceptMsg(testAddr))
        testAddr = 0x348
        self.assertFalse(canbus.acceptMsg(testAddr))
