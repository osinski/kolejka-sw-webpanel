#
# main.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import threading
import logging
import argparse
import canbus
import cancomm

import webapp


if __name__ == '__main__':
    """ Program main function. """
    argparser = argparse.ArgumentParser(
            prog="Kolejka-WebPanel",
            description="Oprogramowanie dla RPi kontrolujące działanie " +
                        "systemu kontroli makiety, udostępniając panel " +
                        "sterujący w formie aplikacji webowej dostępnej " +
                        "na porcie 5000.",
            # usage="",
            add_help=True)
    loggingLevel = 1

    argparser.add_argument('-l', '--log-level', type=int, help='logging.level')

    args = argparser.parse_args()

    loggingLevel = args.log_level * 10
    logging.basicConfig(level=loggingLevel,
                        format='%(levelname)s @ %(asctime)s: %(message)s',
                        datefmt='%H:%M:%S')

    logging.info("Wystartowano program z poziomem logowania = " +
                 f"{logging.getLevelName(loggingLevel)}")

    cancomm.busInit("vcan0")

    if cancomm.isBusAvailable():
        threading.Thread(target=canbus.threadTx).start()
        threading.Thread(target=canbus.threadRx).start()

    threading.Thread(target=cancomm.handleReceived).start()

    logging.info("Start wątku web serwera.")
    threading.Thread(target=webapp.startWebServer).start()

    logging.info("Wyłączanie loggera modułu Flask")
    logging.getLogger('werkzeug').disabled = True
