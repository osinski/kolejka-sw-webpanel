#! /bin/sh

pandoc -f markdown+smart -t html5 \
        docs.md -o ../templates/help.html \
        --metadata pagetitle="Dokumentacja CAN" \
        --metadata author="Marcin Osiński <osinski.marcin.r@gmail.com>" \
        --embed-resources \
        --standalone \
        --css=doc-styles.css \
        --verbose \
        --fail-if-warnings \
        --template pandoc-template.html
