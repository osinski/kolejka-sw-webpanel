# Dokumentacja

## Schemat blokowy systemu

Opracowany system składa się z węzła głównego opartego o *Rapsberry Pi* (od tej
pory określanym w tym dokumencie jako *RPi*) oraz do 9 węzłów roboczych
opartych o dedykowane *PCB* z mikrokontrolerem *Kinetis KE06*
(*MKE06Z128VLK4*). Udostpępniają one 2 interfejsy $I^2 C$. Do każdego z
nich można podłączyć do 4 kart rozszerzeń wejść/wyjść (`I/O`) opartych o
expander *PCAL6524*. Pojedyńczy expander udostępnia 24 dodatkowe `I/O`.

![Schemat Blokowy](./schemat-systemu.svg)

## Panel główny

W celu uzyskania dostępu do panelu sterowania należy w przeglądarce
internetowej wpisać adres *IP* *RPi* z portem `5000`, przykładowo
`192.168.0.100:5000`.

Informację o tym jak można uzyskać adres *IP* *RPi* zamieszczono w
[**Obsługa *RPi***](#obsługa-rpi)


![Panel Główny](./docs-mainpanel.png)



## Dodawanie `I/O`

![Panel Dodawania `I/O`](./docs-mainpanel.png)



## Konfigurowanie wyjść

![Panel Konfigurowania Wyjść](./docs-mainpanel.png)

## Adresowanie węzłów roboczych

Wszystkie *PCB* węzłów roboczych posiadają przełącznik obrotowy (`S2`) służący
do wyboru trybu pracy mikrokontrolera i adresu węzła 
(patrz [schemat adresowania ramek](#schemat-adresowania-ramek)). 
Poniższa tabela przedstawia możliwe opcje konfiguracji za pomocą wspomnianego
przełącznika obrotowego. Pozycja `0` zarezerwowana jest do prac deweloperskich,
testowania nowych funkcjonalności itp. Pozycje `1`-`9` oznaczają normalną
pracę węzła z danym adresem. Pozycje `A` do `F` są zarezerwowane i obecnie
nie pełnią żadnej funkcji.

| Pozycja | Opis                                  |
|---------|---------------------------------------|
| 0       | Dev (nie używać przy normalnej pracy) |
| 1       | Normalna praca, adres = 1             |
| 2       | Normalna praca, adres = 2             |
| 3       | Normalna praca, adres = 3             |
| 4       | Normalna praca, adres = 4             |
| 5       | Normalna praca, adres = 5             |
| 6       | Normalna praca, adres = 6             |
| 7       | Normalna praca, adres = 7             |
| 8       | Normalna praca, adres = 8             |
| 9       | Normalna praca, adres = 9             |
| A (10)  | -                                     |
| B (11)  | -                                     |
| C (12)  | -                                     |
| D (13)  | -                                     |
| E (14)  | -                                     |
| F (15)  | -                                     |

: Dostępne konfiguracje *MCU* za pomocą przełącznika obrotowego `S2`

**UWAGA:** pozycja przełącznika sprawdzana jest tylko przy starcie programu,
późniejsza zmiana pozycji nie ma wpływu na działanie urządzenia!


Każda z kart rozszerzeń podpięta do danego interfejsu $I^2 C$ musi mieć inny
adres. Wynika to ze specyfiki działania magistrali $I^2 C$. Wybór adresu
dokonuje się obsadzając dokładnie jeden `0 Ohm` rezystor z dostępnych
`R2` do `R5`. Poniższa tabela podsumowuje adresowanie kart rozszerzeń `I/O`.

| Rezystor      | Adres      |
|---------------|------------|
| `R2` (*SCL*)  | I2Cn_0     |
| `R3` (*SDA*)  | I2Cn_1     |
| `R4` (*VCC*)  | I2Cn_2     |
| `R5` (*GND*)  | I2Cn_3     |

: Przypisanie adresu kart rozszerzeń



## Komunikacja z wykorzystaniem magistrali *CAN*

### Zasada działania

Cała logika systemu, tj. ustalanie stanu logicznego wyjść znając stan
wejść, odbywa się w *RPi*. Węzły robocze nie decydują o zmianie stanu swoich
wyjść. Mogą one jedynie zgłaszać *RPi* zmianę stanu na swoich wejściach i
oczekiwać na zaktualizowane dane o stanie wyjść.

### Schemat adresowania ramek

Ramki *CAN* w wykorzystywanym systemie używają 11-bitowych adresów (magistrala
*CAN 2.0B* przewiduje także adresy 29-bitowe). Dostępne adresy mieszczą się w
przedziale `0x000` do `0x7FF`. Jak widać w systemie heksadecymalnym potrzeba do
tego 3 cyfr/znaków. Nie wszystkie z dostępnych `2048` adresów są wykorzystane.
Dodatkowo, mechanizm arbitrażu użyty w magistrali *CAN* sprawia, że ramki o
niższym (mniejszym) adresie mają większy priorytet - ramka o adresie `0x140` ma
większy priorytet niż `0x150`.

Poniższa tabela przedstawia użyty schemat adresowania ramek.

| Cyfra od lewej    | 1              | 2                               | 3           |
|-------------------|----------------|---------------------------------|-------------|
| zakres dostępny   | 0-7            | 0-F (0-15)                      | 0-F (0-15)  |
| zakres używany    | 0-3            | zależny od typu (max 0-9 (0-9)) | 1-9 (1-9)   |
| funkcja           | typ wiadomości | podtyp wiadomości               | adres węzła |

: Adresowanie ramek CAN.


### Typy wiadomości

Użyty schemat adresowania przewiduje 4 typy wiadomości:

| Wartość | Nazwa                      | Komentarz                                                               |
|---------|----------------------------|-------------------------------------------------------------------------|
| 0       | Status węzłów              | Węzeł zgłasza RPi czy wszystkie urządzenia do niego podpięte są sprawne |
| 1       | Konfiguracyjne             | Ramki konfiguracyjne                                                    |
| 2       | Komunikacja *RPi* →  Węzeł | RPi wysyła dane do węzła                                                |
| 3       | Komunikacja Węzeł →  *RPi* | Węzeł wysyła dane do RPi                                                |
| 4       | -                          | -                                                                       |
| 5       | -                          | -                                                                       |
| 6       | -                          | -                                                                       |
| 7       | -                          | -                                                                       |

: Typy wiadomości

**Wiadomości statusowe** służą do weryfikacji poprawnego działania węzłów *CAN*
oraz komunikacji węzeł - *RPi*. Węzeł powinien odpowiedzieć wiadomością
statusową za każdym razem gdy odbierze wiadomość konfiguracyjną. Ponadto, węzeł
powinien wysyłać ramkę statusową raz na minutę. Komunikacja z użyciem ramek
statusowych odbywa się tylko w kierunku węzeł → *RPi*.


| Wartość  | Nazwa         | Długość  | Bajt 7 | Bajt 6 | Bajt 5 | Bajt 4 | Bajt 3 | Bajt 2 | Bajt 1 | Bajt 0 |
|----------|---------------|----------|--------|--------|--------|--------|--------|--------|--------|--------|
| 0        | Status Główny | 3        | -      | -      | -      | -      | -      | I2C1   | I2C0   | MCU    |
| 1        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 2        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 3        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 4        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 5        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 6        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 7        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 8        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| 9        | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| A (10)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| B (11)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| C (12)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| D (13)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| E (14)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |
| F (15)   | -             | -        | -      | -      | -      | -      | -      | -      | -      | -      |

: Wiadomości statusowe


| Bit   | 7 | 6 | 5 | 4 | 3        | 2        | 1        | 0        |
|-------|---|---|---|---|----------|----------|----------|----------|
| Pole  | - | - | - | - | I2Cn_3   | I2Cn_2   | I2Cn_1   | I2Cn_0   |

: Bajt I2Cn


| I2Cn_m   | Opis              |
|----------|-------------------|
| 0        | Wszystko OK       |
| 1        | Błąd komunikacji  |

: Opis pól I2Cn_m


| Bit   | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0     |
|-------|---|---|---|---|---|---|---|-------|
| Pole  | - | - | - | - | - | - | - | MCU   |

: Bajt MCU


| MCU    | Opis         |
|--------|--------------|
| 0      | Wszystko OK  |
| 1      | Jakiś błąd   |

: Opis pola MCU



**Wiadomości konfiguracyjne** służą do konfiguracji `I/O` węzłów, tj. które
z używanych `I/O` w węźle są wejściami (`I`), a które wyjściami (`O`).
Komunikacja z użyciem ramek konfiguracyjnych odbywa się tylko w kierunku *RPi*
→ węzeł.

Dane oznaczone sufixem *-U* niosą informację o używanych `I/O`. Wymagane jest,
aby wszystkie bajty z danymi o użyciu `I/O` zostały wysłane.

Dane oznaczone sufixem *-C* niosą informację o roli danego `I/O`, tj. czy jest
ono wejściem czy wyjściem. Nie należy wysyłać danych konfiguracyjnych dla
nieużywanych `I/O`.


| Wartość  | Nazwa       | Długość  | Bajt 7   | Bajt 6   | Bajt 5   | Bajt 4   | Bajt 3   | Bajt 2   | Bajt 1   | Bajt 0   |
|----------|-------------|----------|----------|----------|----------|----------|----------|----------|----------|----------|
| 0        | MCU_IO-U    | 7        | -        | GPIOI-U  | GPIOH-U  | GPIOG-U  | GPIOF-U  | GPIOE-U  | GPIOD-U  | GPIOB-U  |
| 1        | MCU_IO-C    | 0-7      | -        | GPIOI-C  | GPIOH-C  | GPIOG-C  | GPIOF-C  | GPIOE-C  | GPIOD-C  | GPIOB-C  |
| 2        | I2C0_0_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 3        | I2C0_1_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 4        | I2C0_2_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 5        | I2C0_3_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 6        | I2C1_0_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 7        | I2C1_1_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 8        | I2C1_2_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| 9        | I2C1_3_IO   | 3-6      | -        | -        | P2-C     | P1-C     | P0-C     | P2-U     | P1-U     | P0-U     |
| A (10)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |
| B (11)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |
| C (12)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |
| D (13)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |
| E (14)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |
| F (15)   | -           |          | -        | -        | -        | -        | -        | -        | -        | -        |

: Wiadomości konfiguracyjne


| Bit   | 7          | 6          | 5          | 4          | 3          | 2          | 1          | 0          |
|-------|------------|------------|------------|------------|------------|------------|------------|------------|
| Pole  | GPIOX-U_8  | GPIOX-U_7  | GPIOX-U_6  | GPIOX-U_5  | GPIOX-U_4  | GPIOX-U_3  | GPIOX-U_2  | GPIOX-U_1  |

: Bajty GPIOX-U


| GPIOX-U_n   | Opis             |
|-------------|------------------|
| 0           | I/O nie używane  |
| 1           | I/O używane      |

: Opis pól GPIOX-U_n


| Bit   | 7          | 6          | 5          | 4          | 3          | 2          | 1          | 0          |
|-------|------------|------------|------------|------------|------------|------------|------------|------------|
| Pole  | GPIOX-C_8  | GPIOX-C_7  | GPIOX-C_6  | GPIOX-C_5  | GPIOX-C_4  | GPIOX-C_3  | GPIOX-C_2  | GPIOX-C_1  |

: Bajty GPIOX-C


| GPIOX-C_n   | Opis               |
|-------------|--------------------|
| 0           | I/O jest wejściem  |
| 1           | I/O jest wyjściem  |

: Opis pól GPIOX-C_n


| Bit   | 7          | 6          | 5          | 4          | 3          | 2          | 1          | 0          |
|-------|------------|------------|------------|------------|------------|------------|------------|------------|
| Pole  | Px-U_7     | Px-U_6     | Px-U_5     | Px-U_4     | Px-U_3     | Px-U_2     | Px-U_1     | Px-U_0     |

: Bajty Px-U


| Px-U_n      | Opis             |
|-------------|------------------|
| 0           | I/O nie używane  |
| 1           | I/O używane      |

: Opis pól Px-U_n


| Bit   | 7          | 6          | 5          | 4          | 3          | 2          | 1          | 0          |
|-------|------------|------------|------------|------------|------------|------------|------------|------------|
| Pole  | Px-C_7     | Px-C_6     | Px-C_5     | Px-C_4     | Px-C_3     | Px-C_2     | Px-C_1     | Px-C_0     |

: Bajty Px-C


| Px-C_n      | Opis               |
|-------------|--------------------|
| 0           | I/O jest wejściem  |
| 1           | I/O jest wyjściem  |

: Opis pól Px-C_n



**Wiadomości komunikacyjne** służą do wymiany danych pomiędzy *RPi* i węzłami.
Węzły przesyłają do *RPi* stan swoich `I/O` przy każdej zmianie stanu
logicznego na którymkolwiek z ich wejść, natomiast *RPi* wysyła informacje o
stanie logicznym na wyjściach danego węzła.


| Wartość  | Nazwa      | Długość  | Bajt 7 | Bajt 6 | Bajt 5 | Bajt 4 | Bajt 3 | Bajt 2 | Bajt 1 | Bajt 0 |
|----------|------------|----------|--------|--------|--------|--------|--------|--------|--------|--------|
| 0        | MCU_IO     | 1-7      | -      | GPIOI  | GPIOH  | GPIOG  | GPIOF  | GPIOE  | GPIOD  | GPIOB  |
| 1        | I2C0_0_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 2        | I2C0_1_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 3        | I2C0_2_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 4        | I2C0_3_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 5        | I2C1_0_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 6        | I2C1_1_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 7        | I2C1_2_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 8        | I2C1_3_IO  | 1-3      | -      | -      | -      | -      | -      | P2     | P1     | P0     |
| 9        | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| A (10)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| B (11)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| C (12)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| D (13)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| E (14)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |
| F (15)   | -          |          | -      | -      | -      | -      | -      | -      | -      | -      |

: Wiadomości danych


| Bit   | 7        | 6        | 5        | 4        | 3        | 2        | 1        | 0        |
|-------|----------|----------|----------|----------|----------|----------|----------|----------|
| Pole  | GPIOX_8  | GPIOX_7  | GPIOX_6  | GPIOX_5  | GPIOX_4  | GPIOX_3  | GPIOX_2  | GPIOX_1  |

: Bajty GPIOX



| Bit   | 7        | 6        | 5        | 4        | 3        | 2        | 1        | 0        |
|-------|----------|----------|----------|----------|----------|----------|----------|----------|
| Pole  | PX_7     | PX_6     | PX_5     | PX_4     | PX_3     | PX_2     | PX_1     | PX_0     |

: Bajty PX


W przypadku gdy `I/O` jest nieużywane lub ustawione jako wyjście, odpowiadający
mu bit będzie ustawiony jako `0` w ramce danych wysyłanej przez węzeł przy
informowaniu *RPi* o stanie wejść.

W przypadku gdy `I/O` jest nieużywane lub ustawione jako wejście, odpowiadający
mu bit może mieć dowolną wartość w ramce danych wysyłanej przez *RPi* gdy
wysyła ona węzłowi aktualny stan wyjść.


## Obsługa *RPi*

W przypadku problemów z dostępem do panelu głównego należy połączyć się z *RPi*
za pomocą `ssh`.

> **user**: *rysiu*\
> **password**: *kajkoikokosz*\

W celu znalezienia adresu *IP* przypisanego *RPi* (w przypadku sieci z
włączonym *DHCP*) można użyć programu `nmap`.

`nmap -p 22 -sV --open 192.168.0.0/24`

Przeprowadzi skan sieci od adresu `192.168.0.0` do `192.168.0.255` w
poszukiwaniu urządzeń z otwartym portem `22` (port na którym działa `ssh`).

*RPi* zgłasza się w następujący sposób:

> Nmap scan report for 192.168.0.205 (192.168.0.205)\
> Host is up (0.0026s latency).\
> \
> PORT   STATE SERVICE VERSION\
> 22/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)\
> Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel\

Webpanel został skonfigurowany jako *daemon* obsługiwany przez `systemd`.
Uruchamiany jest on automatycznie przy starcie systemu. Poleceniem 

`sudo systemctl status kolejka.service`

można sprawdzić status programu. Przykładowa odpowiedź na to polecenie wygląda
tak:

> ● kolejka.service - Kolejka WebPanel Service\
>    Loaded: loaded (/lib/systemd/system/kolejka.service; enabled; vendor preset: enabled)\
>    Active: active (running) since Mon 2022-10-31 16:10:00 GMT; 16h ago\
>  Main PID: 977 (python3.9)\
>     Tasks: 4 (limit: 779)\
>       CPU: 2min 22.188s\
>    CGroup: /system.slice/kolejka.service\
>            └─977 /usr/bin/python3.9 /home/rysiu/kolejka-sw-webpanel/main.py\

Poniżej znajduje się także kilka linii z logiem. Jego całość można obejrzeć
wywołując polecenie:

`journalctl -u kolejka.service`

lub:

`journalctl -fu kolejka.service`

żeby zacząć przeglądać ostatnie wpisy.


Linia:

>    Active: active (running) since Mon 2022-10-31 16:10:00 GMT; 16h ago

wskazuje na brak błędów - webpanel powinien działać.

**W przypadku problemów należy skopiować log programu i wysłać go na adres:
[osinski.marcin.r@gmail.com](mailto:osinski.marcin.r@gmail.com)**.


## Repozytoria

* [Web-Panel](https://www.gitlab.com/osinski/kolejka-sw-webpanel)

* [Obraz systemu *RPi*]()

* [Firmware dla *Kinetis KE06*](https://www.gitlab.com/osinski/kolejka-sw-can-node)

* [*HW* węzła roboczego](https://www.gitlab.com/osinski/kolejka-hw-io-mcu)

* [*HW* karty rozszerzeń `I/O`](https://www.gitlab.com/osinski/kolejka-hw-io-expander)

* [*HW* karty rozszerzeń *CAN* dla *RPi*](https://www.gitlab.com/osinski/kolejka-hw-rpi-can-shield)

