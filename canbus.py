#
# canbus.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import logging
import queue

from socketcan import CanRawSocket

""" Whether CAN bus is active on the device. """
isActive: bool = False
""" CAN bus socket. """
socket: CanRawSocket = None
""" Size of Tx and Rx Queues. """
_NO_OF_FRAMES = 10
""" Queue holding CAN frames to be transmitted. """
canTxFramesQueue = queue.Queue(maxsize=_NO_OF_FRAMES)
""" Queue holding received CAN frames. """
canRxFramesQueue = queue.Queue(maxsize=_NO_OF_FRAMES)


def acceptMsg(address: int) -> bool:
    correctRanges = [
        (0x001, 0x007),  # status
        (0x301, 0x307),  # data from MCU
        (0x311, 0x317),  # data from I2C0_0
        (0x321, 0x327),  # data from I2C0_1
        (0x331, 0x337),  # data from I2C0_2
        (0x341, 0x347),  # data from I2C0_3
        (0x351, 0x357),  # data from I2C1_0
        (0x361, 0x367),  # data from I2C1_1
        (0x371, 0x377),  # data from I2C1_2
        (0x381, 0x387),  # data from I2C1_3
    ]

    return any(rng[0] <= address <= rng[1] for rng in correctRanges)


def threadRx():
    """ CAN bus receive thread. """
    logging.info("Start wątku Tx magistrali CAN.")
    while isActive:
        recvdFrame = socket.recv()

        logging.info("Odebrano ramkę CAN")
        logging.debug(f"ID ramki: {hex(recvdFrame.can_id)}")
        logging.debug(f"Dane ramki: {[hex(byte) for byte in recvdFrame.data]}")

        if acceptMsg(recvdFrame.can_id):
            logging.info("Zaakceptowane ramkę")
            canRxFramesQueue.put(recvdFrame)


def threadTx():
    """ CAN bus transmit thread. """
    logging.info("Start wątku Rx magistrali CAN.")
    while isActive:
        frame = canTxFramesQueue.get()

        socket.send(frame)
        logging.info("Wysłano ramkę CAN.")
        logging.debug(f"ID ramki: {hex(frame.can_id)}")
        logging.debug(f"Dane ramki: {[hex(byte) for byte in frame.data]}")
