import logging

from socketcan import CanFrame

from cannodes import devices as nodeDevices
from cannodes import updateNodeDeviceStatuses, updateNodeGpioStates


def _checkForStatusFrame(id: int):
    firstChar = (id & 0x700) >> 8
    secondChar = (id & 0x0F0) >> 4
    thirdChar = id & 0x00F

    return (firstChar == 0) and (secondChar == 0) and (1 <= thirdChar <= 9)


def _checkForDataFrame(id: int):
    firstChar = (id & 0x700) >> 8
    secondChar = (id & 0x0F0) >> 4
    thirdChar = id & 0x00F

    return (firstChar == 3) and (secondChar < 9) and (1 <= thirdChar <= 9)


def handleStatusMessage(data: list, nodeAddr: int):
    # TODO: figure out what to do with this data
    EXPECTED_LEN = 3
    actualLen = len(data)

    logging.info("Obsługa ramki statusowej")
    logging.debug(f"Dane ramki statusowej: {[hex(d) for d in data]}")

    if actualLen != EXPECTED_LEN:
        logging.error(
            "Ilość danych w ramce statusowej jest niezgodna ze specyfikacją" +
            f"\r\nSpodziewana ilość: {EXPECTED_LEN}, w ramce: {actualLen}"
        )
        return

    updateNodeDeviceStatuses(nodeAddr, data)


def handleDataMessage(data: list, device: str, nodeAddr: int):
    EXPECTED_LEN_MCU = 7
    EXPECTED_LEN_EXPANDERS = 3
    actualLen = len(data)

    logging.info(
        f"Obsługa ramki z danymi o portach {device} z węzła {nodeAddr}"
    )
    logging.debug(f"Dane ramki danych: {[hex(d) for d in data]}")

    if device == "MCU" and actualLen != EXPECTED_LEN_MCU:
        logging.error(f"""\
Ilość danych w ramce danych jest niezgodna ze specyfikacją
Spodziewana ilość: {EXPECTED_LEN_MCU}, w ramce: {actualLen}
        """)
        return
    elif "Expander" in device and actualLen != EXPECTED_LEN_EXPANDERS:
        logging.error(f"""\
Ilość danych w ramce danych jest niezgodna ze specyfikacją
Spodziewana ilość: {EXPECTED_LEN_EXPANDERS}, w ramce: {actualLen}
        """)
        return

    updateNodeGpioStates(nodeAddr, data)


def dispatchFrame(frame: CanFrame):
    frameID = frame.can_id
    frameData = frame.data

    nodeAddr = frameID & 0x00F
    deviceNumber = (frameID & 0x0F0) >> 4
    nodeDevice = nodeDevices[deviceNumber]

    if _checkForStatusFrame(frameID):
        handleStatusMessage(list(frameData), nodeAddr)
    elif _checkForDataFrame(frameID):
        handleDataMessage(list(frameData), nodeDevice, nodeAddr)
    else:
        logging.error(
            "Próba obsługi ramki o nieprawidłowym typie (najstarszy półbajt)."
        )
