#
# webapp.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import logging

import flask

import cancomm
import cannodes
import ios

"""  Flask Web Server. """
_app = flask.Flask('Kolejka-Webpanel')


def startWebServer():
    """ Start the Web Server. """
    logging.info("Start serwera.")
    _app.run(debug=True, port=5000, host='0.0.0.0', use_reloader=False)


@_app.route('/', methods=['GET'])
@_app.route('/index', methods=['GET'])
def index():
    """ Handle index page. """
    logging.info("Wyświetlanie strony głównej (index).")
    return flask.render_template('index.html')


@_app.route('/mainpanel.html', methods=['GET'])
def mainPage():
    """ Handle main page. """
    logging.info("Wyświetlanie strony głównej.")

    headersStatus = ('Adres Węzła', 'Status MCU',
                     'Status Expanderów I2C0', 'Status Expanderów I2C1',)

    headersLog = ('Kierunek', 'Adres ramki', 'Dane')

    ios.loadIOsFromJSON()
    cannodes.generateListOfNodes(ios.inputsOutputs)
    nodesStatus = cannodes.generateStatusString(cannodes.nodesStatuses)

    return flask.render_template('mainpanel.html',
                                 bus_available=cancomm.isBusAvailable(),
                                 ios_file_found=ios.isCacheFileAvailable(),
                                 headers_status=headersStatus,
                                 headers_log=headersLog,
                                 data_status=nodesStatus,
                                 data_logs="")


@_app.route('/io-add.html', methods=['GET'])
def ioAddPage():
    """ Handle IO add page. """
    logging.info("Wyświetlanie strony dodadawania I/O.")

    headersIO = ['Lp.', 'Nazwa', 'I/O', 'Adres Węzła', 'Urządzenie',
                 'Port_Pin', 'Status', 'Test', 'Usuń']

    return flask.render_template(
        'io-add.html',
        nodeAddresses=cannodes.nodeAddrs,
        devices=cannodes.devices,
        pins=cannodes.portsMCU,
        dirs=['I', 'O'],
        tableHeaders_configuredIO=headersIO,
        tableData_configuredIO=ios.listForJinja([]),
        data_configuredIO=ios.listForJinja(ios.inputsOutputs)
    )


@_app.route('/io/device=<device>', methods=['GET'])
def ioAddPage_fillDeviceCombobox(device: str):
    """
    Respond with port names depending on device to fill comboboxes in the
    Web Panel add IO page.
    """
    dbgMsg = "Odpowiedź serwera z nazwami portów istniejących w wybranym "
    dbgMsg += "urządzeniu."
    logging.debug(dbgMsg)

    if device == 'MCU' or device == '':
        pinsToSend = cannodes.portsMCU
    elif 'Expander' in device:
        pinsToSend = cannodes.portsExpanders
    else:
        pinsToSend = ['-']

    response = [f"<option id={str}>{str}</option>\n"
                for str in pinsToSend]

    return flask.make_response(''.join(response))


@_app.route('/io/<name>/<io>/<nodeAddr>/<device>/<portpin>', methods=['POST'])
def ioAddPage_addNewIO(name, io, nodeAddr, device, portpin):
    """ Handle addition of new IO. """
    dbgMsg = f"Dodawanie IO: {name}, {io}, {nodeAddr}, {device}, {portpin}."
    logging.debug(dbgMsg)

    if io == "O":
        ios.inputsOutputs.insert(0, ios.Output(name=name,
                                               nodeAddr=nodeAddr,
                                               device=device,
                                               portpin=portpin))
    elif io == "I":
        ios.inputsOutputs.insert(0, ios.Input(name=name,
                                              nodeAddr=nodeAddr,
                                              device=device,
                                              portpin=portpin))
    else:
        logging.error("Cannot add IO")
        return flask.make_response('Error')

    ios.saveToFile()
    logging.info("Added new IO.")

    return flask.make_response('OK')


@_app.route('/io/delete/<ioToDelete>', methods=['POST'])
def ioAddPage_removeIO(ioToDelete):
    """ Handle removal of existing IO from the list of IOs. """
    dbgMsg = f"Usuwanie IO o nr {ioToDelete}."
    logging.debug(dbgMsg)

    rowToDelete = int(ioToDelete) - 1
    ios.inputsOutputs.pop(rowToDelete)

    ios.saveToFile()

    return flask.make_response('OK')


@_app.route('/io/test/<ioToTest>', methods=['POST'])
def testIO(ioToTest):
    """ Handle testing of an IO. """
    dbgMsg = "Testowanie IO o nr {ioToTest}"
    logging.debug(dbgMsg)

    ioToTest = int(ioToTest) - 1
    ios.inputsOutputs[ioToTest].tested = not ios.inputsOutputs[ioToTest].tested
    # <refactor> idea - consider modifying the file instead of overwriting its
    # whole contents
    ios.saveToFile()

    return flask.make_response('OK')


@_app.route('/io-configure.html', methods=['GET'])
def ioConfigurePage():
    """ Handle IO configuration page. """
    logging.info("Wyświetlanie strony konfiguracji wejść/wyjść")

    try:
        firstOutput = next(output for output in ios.inputsOutputs if
                           isinstance(output, ios.Output))
    except StopIteration:
        firstOutput = ios.Output()
        logging.warn(
            "Próba konfiguracji wyjść, ale żadne wyjście nie jest zdefiniowane"
        )

    return flask.render_template(
        'io-configure.html',
        outputs=[io.name for io in ios.inputsOutputs
                 if isinstance(io, ios.Output)],
        inputs=[io.name for io in ios.inputsOutputs
                if isinstance(io, ios.Input)],
        enablingConditions=firstOutput.conditionsToEnable,
        blinkingEnablingConditions=firstOutput.conditionsToEnableBlinking,
        disablingConditions=firstOutput.conditionsToDisable,
        blinkingDisablingConditions=firstOutput.conditionsToDisableBlinking
    )


@_app.route('/io/config/<outputName>', methods=['GET'])
def ioConfigurePage_getOutputConditions(outputName):
    """ Respond with requested output behavior conditions. """
    dbgMsg = f"Wysyłanie warunków do włączenia/wyłączenia wyjścia {outputName}."
    logging.debug(dbgMsg)

    output = next(output for output in ios.inputsOutputs if
                  output.name == outputName)

    defaultState = "Aktywne"
    if output.defaultState:
        defaultState = "Aktywne"
    elif output.defaultState:
        defaultState = "Nieaktywne"

    return flask.make_response(output.conditionsToEnable + ";" +
                               output.conditionsToEnableBlinking + ";" +
                               output.conditionsToDisable + ";" +
                               output.conditionsToDisableBlinking + ";" +
                               defaultState)


@_app.route('/io/outputConditions', methods=['POST'])
def ioConfigurePage_setOutputConditions():
    """ Set behavior conditions for all outputs. """
    def conditionsTestParse(codition: str) -> bool:
        """ Test Parse. """
        conditionOK = True

        return conditionOK

    dbgMsg = "Ustawianie nowych warunków włączenia/wyłączenia wyjść."
    logging.debug(dbgMsg)

    jsonData = flask.request.get_json()

    dbgMsg = "Nowe dane: {jsonData}."
    logging.debug(dbgMsg)

    if type(jsonData) is None:
        return flask.make_response('Odebrany JSON jest pusty')

    defaultState = False

    if jsonData['defaultState'] == "Aktywne":
        defaultState = True

    for o in ios.inputsOutputs:
        if o.name != jsonData['output']:
            continue
        elif jsonData['toEnable']:
            o.conditionsToEnable = jsonData['toEnable']
        elif jsonData['toEnableBlinking']:
            o.conditionsToEnableBlinking = jsonData['toEnableBlinking']
        elif jsonData['toDisable']:
            o.conditionsToDisable = jsonData['toDisable']
        elif jsonData['toDisableBlinking']:
            o.conditionsToDisableBlinking = jsonData['toDisableBlinking']

        o.defaultState = defaultState
        break

    ios.saveToFile()

    return flask.make_response('Ustawienia odebrane\r\n')


@_app.route('/help.html', methods=['GET'])
def helpPage():
    """ Handle help page. """
    logging.info("Wyświetlanie strony pomocy.")
    return flask.render_template('help.html')


@_app.route('/sendConfigsToNodes', methods=['POST'])
def sendConfigsToNodes():
    """ Send config information to nodes via CAN bus. """
    logging.info("Wysyłanie konfiguracji do węzłów.")

    _BYTES_IN_FRAME = 8

    for node in cannodes.nodesStatuses:
        # loop over expanders
        # get expander keys
        expanders = list(node.GPIOs)[1:]
        frameID = 1 * 16**2 + node.address
        for expanderNumber, expanderName in enumerate(expanders):
            if node.statuses[expanderName] is cannodes.DEVICE_STATUS.UNUSED:
                # Don't send configs to device that's not used
                continue

            frameData = [0x00 for _ in range(_BYTES_IN_FRAME)]
            for portNumber, portName in enumerate(node.GPIOs[expanderName]):
                # Loop over Expanders Ports
                byte = 0x00
                for pinNumber, pinValue in\
                        enumerate(node.GPIOs[expanderName][portName]):
                    # Loop over Expanders Ports Pins
                    if pinValue is not cannodes.IO_CONF.UNUSED:
                        byte |= (1 << pinNumber)
                frameData[portNumber] = byte
            for portNumber, portName in enumerate(node.GPIOs[expanderName]):
                # Loop over Expanders Ports
                byte = 0x00
                for pinNumber, pinValue in\
                        enumerate(node.GPIOs[expanderName][portName]):
                    # Loop over Expanders Ports Pins
                    if pinValue is cannodes.IO_CONF.OUTPUT:
                        byte |= (1 << pinNumber)
                frameData[portNumber + 3] = byte

            frameID = 1 * 16**2 + (2 + expanderNumber) * 16**1 + node.address
            cancomm.scheduleToSend(frameID, frameData)

        dataConfig = [0x00 for _ in range(_BYTES_IN_FRAME)]
        dataIO = [0x00 for _ in range(_BYTES_IN_FRAME)]
        for portNumber, portName in enumerate(node.GPIOs["MCU"]):
            # Loop over MCU GPIO Ports
            byteConfig = 0x00
            byteIO = 0x00
            for pinNumber, pinValue in enumerate(node.GPIOs["MCU"][portName]):
                # Loop over MCU GPIO Ports Pins
                if pinValue is not cannodes.IO_CONF.UNUSED:
                    byteConfig |= (1 << pinNumber)
                if pinValue is cannodes.IO_CONF.OUTPUT:
                    byteIO |= (1 << pinNumber)
            dataConfig[portNumber] = byteConfig
            dataIO[portNumber] = byteIO

        frameID = 1 * 16**2 + 0 * 16**1 + node.address
        cancomm.scheduleToSend(frameID, dataConfig)

        frameID = 1 * 16**2 + 1 * 16**1 + node.address
        cancomm.scheduleToSend(frameID, dataIO)

    return flask.make_response('OK')


@_app.route('/getCANFrames', methods=['GET'])
def displayTrafficOnCANBus():
    """
    Respond with string representing traffic on CAN Bus.
    """
    traffic = cancomm.trafficToString()
    logging.debug(f"Wysyłanie ruchu na magistrali CAN:\r\n{traffic}.")

    return flask.make_response(traffic)
