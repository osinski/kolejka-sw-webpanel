#
# ios.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import logging
from dataclasses import dataclass
from enum import Enum

from dataclasses_json import dataclass_json


class PIN_STATE(Enum):
    """ Enum-like class to represent logic states a GPIO can be in. """
    LOW, HIGH = range(2)


@dataclass_json
@dataclass
class InputOutput:
    """ Abstract dataclass to represent IOs in the system. """
    name: str = ""
    nodeAddr: int = 1
    device: str = ""
    portpin: str = ""


@dataclass_json
@dataclass
class Input(InputOutput):
    """
    Dataclass inheriting from InputOutput to represent inputs in the system.
    """
    state: PIN_STATE = PIN_STATE.LOW


@dataclass_json
@dataclass
class Output(InputOutput):
    """
    Dataclass inheriting from InputOutput to represent outputs in the system.
    """
    state: PIN_STATE = PIN_STATE.LOW
    conditionsToEnable: str = ""
    conditionsToEnableBlinking: str = ""
    conditionsToDisable: str = ""
    conditionsToDisableBlinking: str = ""
    defaultState: PIN_STATE = PIN_STATE.LOW
    tested: bool = False


""" List holding every IO defined in the system. """
inputsOutputs: list[InputOutput] = []

""" Boolean determining if ios.json file was found """
_cacheFileFound: bool = False


def isCacheFileAvailable() -> bool:
    return _cacheFileFound


def saveToFile():
    """ Save inputsOutputs IOs list to the JSON file. """

    def listToJSON(iosList: list[InputOutput]) -> str:
        """ Helper function to convert list of IOs into a JSON string. """

        result: str = ''
        for io in iosList:
            result += io.to_json()
            result += '\n'

        return result

    with open('ios.json', 'w') as file:
        file.write(listToJSON(inputsOutputs))

    logging.info("Zapisano IO do pliku JSON.")


def loadIOsFromJSON():
    """ Parse JSON file for IOs and create inputsOutputs list. """
    global _cacheFileFound

    inputsOutputs.clear()
    try:
        with open('ios.json') as file:
            lines = file.readlines()
            for line in lines:
                if "defaultState" in line:
                    inputsOutputs.append(Output.from_json(line))
                else:
                    inputsOutputs.append(Input.from_json(line))
        _cacheFileFound = True
        logging.info("Wczytano IO z pliku JSON")
    except FileNotFoundError:
        _cacheFileFound = False
        logging.warning("Nie znaleziono pliku ios.json")


def listForJinja(iosList: list[InputOutput]) -> list[list[str]]:
    """
    Turn given iosList into a list of strings to be displayed in the webpage.
    """

    result = []
    for io in iosList:
        ioList = list(io.to_dict().values())

        iofield = ""
        if isinstance(io, Output):
            iofield = "O"
        elif isinstance(io, Input):
            iofield = "I"

        # in case of input ioList[4] == ioList[-1], so it gets included twice
        # and is a bit hacky/ugly, consider during <refactor>
        result.append([ioList[0]] + [iofield] + ioList[1:5] + [ioList[-1]])

        if result[-1][-1] is True:
            result[-1][-1] = '1'
        elif result[-1][-1] is False:
            result[-1][-1] = '0'

    logging.debug(f"listForJinja result: {result}")

    return result
