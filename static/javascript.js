//
// javascript.js
// Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
//
// Distributed under terms of the MIT license.
//

var interval_getCANFrames;
var showStartupMessage = true;

window.onload = function() {
    document.getElementById("mainpanel_MenuButton").onclick = 
        loadPage_MainPanel;
    document.getElementById("io-add_MenuButton").onclick = 
        loadPage_IO_add;
    document.getElementById("io-configure_MenuButton").onclick = 
        loadPage_IO_configure;
    //document.getElementById("help-documentation_MenuButton").onclick=
    //    loadPage_help;

    loadPage_MainPanel();
}


function getCANFrames() {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("GET", 'getCANFrames', true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                document.getElementById("dataDiv_logs")
                    .dataset.data = req.responseText;

                tableLog.clear();
                tableLog.extractDataFromDataDiv();
                tableLog.updateItemCount(
                    document.getElementById("dataDiv_logs")
                        .dataset.data.split(',').length / 3
                );
                tableLog.display();
            }
        }
        req.send();
    }
}

function loadPage_MainPanel() {
    loadPage("mainpanel.html");

    interval_getCANFrames = window.setInterval(getCANFrames, 1000)
    return false;
}

function loadPage_IO_add() {
    loadPage("io-add.html");

    window.clearInterval(interval_getCANFrames);
    return false;
}

function loadPage_IO_configure() {
    loadPage("io-configure.html");

    window.clearInterval(interval_getCANFrames);
    return false;
}

function loadPage_help() {
    loadPage("help.html");

    window.clearInterval(interval_getCANFrames);
    return false;
}

function loadPage(page) {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", page, true);
    xmlhttp.setRequestHeader("Content-type",
        "application/x-www-form-urlencoded");
    xmlhttp.send();

    xmlhttp.onreadystatechange = function() {
        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
            document.getElementById("content").innerHTML = xmlhttp.responseText;
            // TODO: split this shit into separate functions
            if (page === "mainpanel.html") {
                const statusTableServerDataPerRow = 4;
                const statusTableMaxRowsPerPage = 9;
                const logTableServerDataPerRow = 3;
                const logTableMaxRowsPerPage = 10;

                tableStatus = new PSW_Table(
                    "table-status",
                    document.getElementById("tableStatus"),
                    null,
                    document.getElementById("dataDiv_status"),
                    statusTableMaxRowsPerPage,
                    statusTableServerDataPerRow,
                    fillRow_tableStatus,
                    null
                );
                tableLog = new PSW_Table(
                    "table-logs",
                    document.getElementById("tableLogs"),
                    null,
                    document.getElementById("dataDiv_logs"),
                    logTableMaxRowsPerPage,
                    logTableServerDataPerRow,
                    fillRow_tableLog,
                    null
                );
                tableStatus.updateItemCount(
                    document.getElementById("dataDiv_status")
                        .dataset.data.split(',').length /
                        statusTableServerDataPerRow
                );
                tableStatus.display();

                tableLog.updateItemCount(
                    document.getElementById("dataDiv_logs")
                        .dataset.data.split(',').length /
                        logTableServerDataPerRow
                );
                tableLog.display();

                if (showStartupMessage) {
                    var startupMsg = "";
                    var isBusAvailable = 
                        document.getElementById("dataDiv_busAvailable")
                        .dataset.data;
                    var isIOsFileAvailable = 
                        document.getElementById("dataDiv_iosFileFound")
                        .dataset.data;

                    if (isBusAvailable === "True") {
                        startupMsg = "Wykryto interfejs CAN!\r\n";
                    } else {
                        startupMsg = "Nie wykryto interfejsu CAN!\r\n";
                        startupMsg += "Działanie w trybie demo.\r\n";
                    }
                    if (isIOsFileAvailable === "True") {
                        startupMsg += "Znaleziono plik \"ios.json\".\r\n";
                        startupMsg += "Zdefiniowane wcześniej wejścia/wyjścia "
                        startupMsg += "będą dostępne.\r\n"
                    } else {
                        startupMsg += "Nie znaleziono pliku \"ios.json\".\r\n"
                        startupMsg += "Plik ten zostanie utworzony przy "
                        startupMsg += "definiowaniu/edycji wejść/wyjść."
                    }

                    showStartupMessage = false;
                    window.alert(startupMsg);
                }

            } else if (page === "io-add.html") { 
                let dataDiv = document.getElementById("dataDiv_configuredIO");
                const ioTableServerDataPerRow = 7;
                const ioTableMaxRowsPerPage = 10;

                if (dataDiv.dataset.data.split(',').length == 1) {
                    addedIOs = 0;
                } else {
                    addedIOs = dataDiv.dataset.data.split(',')
                                .length / ioTableServerDataPerRow;
                }

                tableIO = new PSW_Table(
                    "table-IOs",
                    document.getElementById("tableIOs"),
                    document.getElementById("label_ioTableControlItemCount"),
                    dataDiv,
                    ioTableMaxRowsPerPage,
                    ioTableServerDataPerRow,
                    fillRow_tableIO,
                    updateIDs_tableIO
                );
                tableIO.updateItemCount(addedIOs);
                tableIO.display();
                document.getElementById("textAreaIOtableFiltering").
                    addEventListener("keyup", filter_tableIO);
            } else if (page === "io-configure.html") {
                var inputs = [];
                var outputs = [];

                var inputsStr = 
                    document.getElementById("dataDiv_inputs").dataset.inputs.
                    split(',');
                var outputsStr = 
                    document.getElementById("dataDiv_outputs").dataset.outputs.
                    split(',');

                for (let i = 0; i < inputsStr.length; i++) {
                    inputs.push({
                        key: inputsStr[i].replace(/[^\w]/g, ''),
                        value: inputsStr[i].replace(/[^\w]/g, '')
                    });
                }
                for (let i = 0; i < outputsStr.length; i++) {
                    outputs.push({
                        key: outputsStr[i].replace(/[^\w]/g, ''),
                        value: outputsStr[i].replace(/[^\w]/g, '')
                    });
                }

                var tributeEnableTextArea = new Tribute({
                    collection: [
                    {
                        trigger: 'I{',
                        values: inputs,
                        selectTemplate: function (item) {
                            return 'I{' + item.original.value + ',';
                        },
                    },
                    {
                        trigger: 'O{',
                        values: outputs,
                        selectTemplate: function (item) {
                            return 'O{' + item.original.value + ',';
                        },
                    }]});
                tributeEnableTextArea.attach(
                    document.getElementById("textareaEnable"));

                var tributeDisableTextArea = new Tribute({
                    collection: [
                    {
                        trigger: 'I{',
                        values: inputs,
                        selectTemplate: function (item) {
                            return 'I{' + item.original.value + ',';
                        },
                    },
                    {
                        trigger: 'O{',
                        values: outputs,
                        selectTemplate: function (item) {
                            return 'O{' + item.original.value + ',';
                        },
                    }]});
                tributeEnableTextArea.attach(
                    document.getElementById("textareaDisable"));
            }
        }
    }
}

// mainpanel.html functions

var tableStatus;
var tableLog;

function sendConfigsToNodes() {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", 'sendConfigsToNodes', true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                //window.alert(req.responseText);
            }
        }
        req.send();
    }

}



// io-add.html functions 

var addedIOs = 0;
var deleteButtons = [];
var tableIO;

function addTestSwitch(tableCell, number, isChecked) {
    var testSwitch = document.createElement('input');
    var testLabel = document.createElement('Label');
    var testSpan = document.createElement('Span');

    testLabel.className = "switch";
    testSpan.className = "slider round";
    testSwitch.type = "checkbox";
    testSwitch.id = "testSwitch_" + number;
    testSwitch.checked = isChecked;
    testSwitch.onclick = testIO;
    tableCell.appendChild(testLabel);
    testLabel.appendChild(testSwitch);
    testLabel.appendChild(testSpan);
}

function addDeleteButton(tableCell, number) {
    var deleteButton = document.createElement('input');

    deleteButton.type = "button";
    deleteButton.className = "deleteButton";
    deleteButton.id = "deleteButton_" + number;
    deleteButton.value = "X";
    deleteButton.onclick = deleteIO;
    tableCell.appendChild(deleteButton);
    deleteButtons.push(deleteButton);
}

function filter_tableIO(event) {
    if (event.key == "Enter") {
        tableIO.filterContent(
            document.getElementById("textAreaIOtableFiltering").value
        );
    }
}

function updateIDs_tableIO(table, idOffset) {
    var id = 0;
    for (let i = 1; i <= table.rows.length-1; i++) {
        id = i + idOffset;
        deleteButtons[i-1].id = "deleteButton_" + id;
        table.rows[i].cells[0].innerHTML = id; 
        table.rows[i].cells[8].textContent = '';
        table.rows[i].cells[8].appendChild(deleteButtons[i-1]);
    }
}

function fillRow_tableIO(tableRow, tableRowIndex, rowID, data, dataRowIndex) {
    for (let i = 0; i < 9; i++) {
        let cell = tableRow.insertCell(i);
        cell.className = "dataTable_cell";
        if ((i == 7) && (tableRow.cells[2].innerHTML === "O")) {
            isIOtested = true;
            // condition in int is hacky because of the way the data is 'moved'
            // from python to js - in js it's literally string representation
            // of the python list, something like __repr__ would return
            // this is the case in every place that data put into dataDiv by
            // python (jinja) is placed
            if (parseInt(data[dataRowIndex + 6][2]) == 0)
                isIOtested = false;
            addTestSwitch(cell, rowID, isIOtested); 
        } else if (i == 7) {
            cell.innerHTML = "";
        } else if (i == 8) {
            addDeleteButton(cell, rowID);
        } else if (i == 0) {
            cell.innerHTML = rowID;
        } else {
            cell.innerHTML = data[dataRowIndex + i-1].replace(/[^\w]/g, '');
        }
    }
}

function fillRow_tableStatus(tableRow, tableRowIndex, rowID, data, dataRowIndex) {
    for (let i = 0; i < 4; i++) {
        let cell = tableRow.insertCell(i);
        cell.className = "dataTable_cell";
        cell.innerHTML = data[dataRowIndex + i];
    }
    //window.alert(data);
}

function fillRow_tableLog(tableRow, tableRowIndex, rowID, data, dataRowIndex) {
    for (let i = 0; i < 3; i++) {
        let cell = tableRow.insertCell(i);
        cell.className = "dataTable_cell";
        cell.innerHTML = data[dataRowIndex + i];
    }
    //window.alert(data);
}

function testIO(event) {
    const btnId = (event.target || event.srcElement).id;
    const rowIndex = parseInt(btnId.split('_')[1]);
    var button = document.getElementById(btnId);
    console.log(button.checked);
    var data = document.getElementById("dataDiv_configuredIO").
                dataset.data.split(',');

    // update table datadiv with info about switch state
    console.log(rowIndex);
    console.log(data[rowIndex * tableIO.getServerDataCellsPerRow() + 6]);
    console.log(document.getElementById("dataDiv_configuredIO"));
    if (button.checked) {
        // info about output being tested is in cell no 6
        data[(rowIndex-1) * tableIO.getServerDataCellsPerRow() + 6] =
            data[(rowIndex-1) * tableIO.getServerDataCellsPerRow() + 6]
            .replace(/[01]/, '1');
    } else {
        data[(rowIndex-1) * tableIO.getServerDataCellsPerRow() + 6] =
            data[(rowIndex-1) * tableIO.getServerDataCellsPerRow() + 6]
            .replace(/[01]/, '0');
    }

    document.getElementById("dataDiv_configuredIO").dataset.data = data.join(',');

    console.log(document.getElementById("dataDiv_configuredIO"));
    tableIO.extractDataFromDataDiv();

    var req = false;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", 'io/test/' + rowIndex, true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                window.alert("testowanie wyjscia o Lp. = " + rowIndex);
            }
        }
        req.send();
    }
}

function deleteIO(event) {
    const btnId = (event.target || event.srcElement).id;
    const rowIndex = parseInt(btnId.split('_')[1]);
    const tableRowIndex = rowIndex % tableIO.getMaxRowsPerPage();
    const table = document.getElementById("tableIOs");
    let data = document.getElementById("dataDiv_configuredIO").
                dataset.data.split(',');
    let tableLen = table.rows.length - 1; // -1 because of header

    tableIO.updateItemCount(--addedIOs);

    table.deleteRow(tableRowIndex);
    deleteButtons.splice(tableRowIndex,1);

    data.splice((rowIndex-1)*tableIO.getServerDataCellsPerRow(),
                tableIO.getServerDataCellsPerRow());
    document.getElementById("dataDiv_configuredIO").dataset.data = data.join(',');

    tableIO.extractDataFromDataDiv();
    tableIO.updatePageNumber();

    let currentPage = tableIO.getPage();
    if (currentPage < tableIO.getNumberOfPages()) {
        tableIO.fillRow(tableLen);
        tableIO.updateIDs();
        tableIO.updateItemCountLabel();
    } else if (currentPage > 1 && tableLen ==1) {
        tableIO.setPreviousPage();
    } else {
        // tableIO.#rowsOnPage--;
        tableIO.updateDisplayedRowsCount();

        tableIO.updateIDs();
        tableIO.updateItemCountLabel();
    }

    var req = false;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", 'io/delete/' + rowIndex, true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                window.alert('Usunięto IO: ' + rowIndex)
            }
        }
        req.send();
    }
}

function addIO() {
    let req = false;
    let name = document.getElementById("io_nameInput").value;
    let io = document.getElementById("directions").value;
    let nodeAddress = document.getElementById("nodeAddress").value;
    let device = document.getElementById("device").value;
    let portpin = document.getElementById("pins").value;

    let table = document.getElementById("tableIOs");
    let dataDiv = document.getElementById("dataDiv_configuredIO");
    let data = dataDiv.dataset.data.split(',');

    const infoCellsPerRow = tableIO.getServerDataCellsPerRow();

    if (name === "") {
        window.alert("Nazwa nie może być pusta");
        return;
    }

    for (let i = 0; i < addedIOs; i++) {
        if (data[i*infoCellsPerRow + 0].replace(/[^\w]/g,'') === name) {
            window.alert("Nazwa IO jest już zajęta przez IO \"" + 
                data[i*infoCellsPerRow+0].replace(/[^\w]/g,'') + "\"");
            return;
        }
        if ((data[i*infoCellsPerRow + 4].replace(/[^\w]/g,'') === portpin) &&
            (data[i*infoCellsPerRow + 3].replace(/[^\w]/g,'') === device) &&
            (data[i*infoCellsPerRow + 2].replace(/[^\w]/g,'') === nodeAddress))
        {
            window.alert("Port_Pin " + portpin + " w urządzeniu " +
                         device + " w węźle o adresie " + nodeAddress +
                         " już istnieje");
            return;
        }
    }

    let newData = [name,io,nodeAddress,device,portpin,'0','0'];
    dataDiv.dataset.data = newData.concat(data);
    tableIO.extractDataFromDataDiv();

    tableIO.updateItemCount(++addedIOs);

    if (tableIO.getPage() > 1) {
        tableIO.setPage(1);
    } else {
        if (tableIO.isFull()) {
            table.deleteRow(table.rows.length-1);
        }

        tableIO.fillRow(1);

        // refactor idea - needing to update this manually may not be a good
        // idea
        tableIO.updateIDs();
        tableIO.updateDisplayedRowsCount();
        tableIO.updateItemCountLabel();
    }

    // send info about new IO to the server
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST",
                "io/" + name +
                "/" + io +
                "/" + nodeAddress +
                "/" + device +
                "/" + portpin,
                true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                window.alert('Wysłano')
            }
        }
        req.send();
    }
}

function io_pinDirChanged(combobox) {
}

function io_nodeAddrChanged(combobox) {
}

function io_deviceChanged(combobox) {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("GET", "io/device=" + combobox.value, true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                document.getElementById("pins").innerHTML = req.responseText;
            }
        }
        req.send();
    }
}

function io_portPinChanged(combobox) {
}

function io_getPortPins(device) {
    
}

function io_nameKeyUp(input) {
    if (input.value.indexOf(" ") > -1) {
        window.alert('Nie używaj spacji!');
        // now delete that space
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf(",") > -1) {
        window.alert('Nie używaj przecinków!');
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf(".") > -1) {
        window.alert('Nie używaj kropek!');
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf("/") > -1) {
        window.alert('Nie używaj ukośników!');
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf("\\") > -1) {
        window.alert('Nie używaj ukośników!');
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf("+") > -1) {
        window.alert('Nie używaj plusa! Rozważ użycie sufiksu _p');
        input.value = input.value.slice(0,-1);
    } else if (input.value.indexOf("-") > -1) {
        window.alert('Nie używaj minusa! Rozważ użycie sufiksu _m');
        input.value = input.value.slice(0,-1);
    }
}



// io-configure functions

function io_outputSelected(outputSelected) {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("GET", "io/config/" + outputSelected.value, true);
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                var response = req.responseText.split(";");
                document.getElementById("textareaEnable").value = 
                    response[0];
                document.getElementById("textareaBlinkingEnable").value = 
                    response[1];
                document.getElementById("textareaDisable").value = 
                    response[2];
                document.getElementById("textareaBlinkingDisable").value = 
                    response[3];
                document.getElementById("defaultState").value =
                    response[4];
            }
        }
        req.send();
    }
}

function addIOConfig() {
    var req = false;

    var output = document.getElementById("outputs").value;
    var toEnable = document.getElementById("textareaEnable").value;
    var toEnableBlinking = document.getElementById("textareaBlinkingEnable").value
    var toDisable = document.getElementById("textareaDisable").value;
    var toDisableBlinking = document.getElementById("textareaBlinkingDisable").value
    var defaultState = document.getElementById("defaultState").value;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", "io/outputConditions", true);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        req.onreadystatechange = function() {
            if ((req.readyState == 4) && (req.status == 200)) {
                window.alert(req.responseText);
            }
        }
        var data = JSON.stringify({"output": output,
                                   "toEnable": toEnable,
                                   "toEnableBlinking": toEnableBlinking,
                                   "toDisable": toDisable,
                                   "toDisableBlinking": toDisableBlinking,
                                   "defaultState": defaultState});
        req.send(data);
    }
}

