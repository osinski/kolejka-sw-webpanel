//
// table.js
// Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
//
// Distributed under terms of the MIT license.
//

class PSW_Table {
    #currentPage = 1;
    #numberOfPages = 1;
    #rowsOnPage = 0;
    #noOfItems = 0;
    #data;
    #filteredIndexes = [];
    constructor(name, table, itemCountLabel, dataDiv, maxRowsPerPage,
                serverDataPerRow,
                fillRowFunction, updateIDsFunction) {
        this.name = name;
        this.table = table;
        this.itemCountLabel = itemCountLabel;
        this.dataDiv = dataDiv;
        this.maxRowsPerPage = maxRowsPerPage;
        this.serverDataPerRow = serverDataPerRow;
        this.fillRowFunction = fillRowFunction;
        this.updateIDsFunction = updateIDsFunction;
        this.extractDataFromDataDiv();
    }
    extractDataFromDataDiv() {
        this.#data = this.dataDiv.dataset.data.split(',');
        if (this.#data.length > 1 &&
            this.#data.length % this.serverDataPerRow != 0) {
                window.alert("Długość danych w datadiv " + this.name + 
                             " się nie zgadza (" + this.#data.length + ")");
        }
    }
    updateItemCount(noOfItems) {
        this.#noOfItems = noOfItems;
    }
    filterContent(filterString) {
        if (filterString === "") {
            this.extractDataFromDataDiv();
            this.#noOfItems = this.#data.length / this.serverDataPerRow;
            this.#filteredIndexes = [];
        } else {
            const names = this.#data.filter(function(val, idx, Arr) {
                return idx % this.serverDataPerRow == 0;
            }, this);

            this.#filteredIndexes = [];
            for (let i = 0; i < names.length; i++) {
                if (names[i].includes(filterString)) {
                    this.#filteredIndexes.push(i);
                }
            }

            let filteredData = [];
            for (let i of this.#filteredIndexes) {
                for (let j = 0; j < this.serverDataPerRow; j++) {
                    filteredData.push(this.#data[i*this.serverDataPerRow + j]);
                }
            }
            this.#data = filteredData;
            this.#noOfItems = filteredData.length / this.serverDataPerRow;
        }
        this.clear();
        this.setPage(1);
    }
    display() {
        this.#numberOfPages = Math.ceil(this.#noOfItems/this.maxRowsPerPage);

        this.updateDisplayedRowsCount();

        for (let i = 1; i <= this.#rowsOnPage; i++) {
            this.fillRow(i);
        }
        if (this.updateIDsFunction != null) {
            // WTF?
            this.updateItemCountLabel();
        }
    }
    updateDisplayedRowsCount() {
        if (this.#noOfItems - this.maxRowsPerPage*(this.#currentPage-1) <= 
                this.maxRowsPerPage) {
            this.#rowsOnPage = 
                this.#noOfItems - this.maxRowsPerPage*(this.#currentPage-1);
        } else {
            this.#rowsOnPage = this.maxRowsPerPage;
        }
    }
    updateItemCountLabel() {
        let n = 0;
        if (this.#rowsOnPage != 0) n = 1;

        this.itemCountLabel.innerHTML =
            (this.maxRowsPerPage*(this.#currentPage-1) + n) + " ... " +
            (this.maxRowsPerPage*(this.#currentPage-1) + this.#rowsOnPage) +
            " / " + this.#noOfItems;
    }
    updateIDs() {
        this.updateIDsFunction(this.table, 
                               this.maxRowsPerPage*(this.#currentPage-1)
        );
    }
    updatePageNumber() {
        this.#numberOfPages = Math.ceil(this.#noOfItems/this.maxRowsPerPage);
    }
    fillRow(indexRowToFill) {
        let row = this.table.insertRow(indexRowToFill);
        row.className = "dataTable_row";
        let indexData = (this.maxRowsPerPage*(this.#currentPage-1) +
                        (indexRowToFill-1)) * this.serverDataPerRow;
        let id = 0;
        if (this.#filteredIndexes.length != 0) {
            id = this.#filteredIndexes[indexRowToFill-1] + 1 +
                    (this.#currentPage-1)*10;
        } else {
            id = indexRowToFill + (this.#currentPage-1) * 10;
        }
        this.fillRowFunction(row, 
                             indexRowToFill,
                             id,
                             this.#data,
                             indexData
        );
    }
    clear() {
        const noOfRowsToDelete = this.table.rows.length;
        for (let i = 1; i < noOfRowsToDelete; i++) {
            this.table.deleteRow(1);
        }
    }
    setPreviousPage() {
        if (this.#currentPage > 1) {
            this.#currentPage--;
            this.clear();
            this.display();
        }
    }
    setNextPage() {
        if (this.#currentPage < this.#numberOfPages) {
            this.#currentPage++;
            this.clear();
            this.display();
        }
    }
    setPage(pageNumber) {
        if (1 <= pageNumber && this.#numberOfPages >= pageNumber) {
            this.#currentPage = pageNumber;
            this.clear();
            this.display();
        }
    }
    getPage() {
        return this.#currentPage;
    }
    getNumberOfPages() {
        return this.#numberOfPages;
    }
    getMaxRowsPerPage() {
        return this.maxRowsPerPage
    }
    getServerDataCellsPerRow() {
        return this.serverDataPerRow;
    }
    isFull() {
        if (this.table.rows.length == this.maxRowsPerPage + 1) {
            return true;
        } else {
            return false;
        }
    }
}
