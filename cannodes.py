#
# cannodes.py
# Copyright (C) 2022 Marcin Osiński <osinski.marcin.r@gmail.com>
#
# Distributed under terms of the MIT license.
#

import logging
from dataclasses import dataclass, field

import ios


_MAX_NUMBER_OF_NODES = 9
_NO_OF_EXPANDERS_PER_BUS = 4
_PINS_IN_MCU_GPIOB = 4
_PINS_IN_MCU_GPIOD = 8
_PINS_IN_MCU_GPIOE = 6
_PINS_IN_MCU_GPIOF = 8
_PINS_IN_MCU_GPIOG = 8
_PINS_IN_MCU_GPIOH = 8
_PINS_IN_MCU_GPIOI = 5
_PINS_IN_EXPANDER_PORTS = 8

nodeAddrs = [str(i+1) for i in range(_MAX_NUMBER_OF_NODES)]

devices = \
    ['MCU'] + \
    ['I2C0_Expander' + str(i) for i in range(_NO_OF_EXPANDERS_PER_BUS)] + \
    ['I2C1_Expander' + str(i) for i in range(_NO_OF_EXPANDERS_PER_BUS)]

portsMCU = \
    ['B_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOB + 1)] + \
    ['D_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOD + 1)] + \
    ['E_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOE + 1)] + \
    ['F_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOF + 1)] + \
    ['G_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOG + 1)] + \
    ['H_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOH + 1)] + \
    ['I_' + str(i) for i in range(1, _PINS_IN_MCU_GPIOB + 1)]

portsExpanders = \
    ['P0_' + str(i) for i in range(1, _PINS_IN_EXPANDER_PORTS + 1)] + \
    ['P1_' + str(i) for i in range(1, _PINS_IN_EXPANDER_PORTS + 1)] + \
    ['P2_' + str(i) for i in range(1, _PINS_IN_EXPANDER_PORTS + 1)]


class IO_CONF:
    """
    Enum-like class to represent possible configs of a GPIO pin
    """
    INPUT, OUTPUT, UNUSED = range(3)


class IO_STATE:
    """
    Enum-like class to represent possible configs of a GPIO pin
    """
    LOW, HIGH = range(2)


class DEVICE_STATUS:
    """
    Enum-like class to represent status of nodes and devices connected to CAN
    bus
    """
    OK, ERROR, UNKNOWN, UNUSED = range(4)


class GPIO:
    config: IO_CONF = IO_CONF.UNUSED
    logicState: IO_STATE = IO_STATE.LOW

    def __init__(self, cfg=IO_CONF.UNUSED, state=IO_STATE.LOW):
        self.config = cfg
        self.logicState = state

    def __str__(self):
        return f'Cfg: {self.config} State: {self.logicState}'

    def __repr__(self):
        return f'Cfg: {self.config} State: {self.logicState}'


@dataclass(eq=False)
class NodeDeviceStatus:
    """
    Dataclass representing CANbus Node Device, what address it has, what
    devices are connected to it and what state are its GPIOs in
    """
    address: int = 0
    statuses: dict = field(
        default_factory=lambda: {
            "MCU": DEVICE_STATUS.UNUSED,
            "I2C0_Expander0": DEVICE_STATUS.UNUSED,
            "I2C0_Expander1": DEVICE_STATUS.UNUSED,
            "I2C0_Expander2": DEVICE_STATUS.UNUSED,
            "I2C0_Expander3": DEVICE_STATUS.UNUSED,
            "I2C1_Expander0": DEVICE_STATUS.UNUSED,
            "I2C1_Expander1": DEVICE_STATUS.UNUSED,
            "I2C1_Expander2": DEVICE_STATUS.UNUSED,
            "I2C1_Expander3": DEVICE_STATUS.UNUSED
        })

    GPIOs: dict[dict[list[GPIO]]] = field(
        default_factory=lambda: {
            "MCU": {
                "GPIOB": [GPIO() for _ in range(_PINS_IN_MCU_GPIOB)],
                "GPIOD": [GPIO() for _ in range(_PINS_IN_MCU_GPIOD)],
                "GPIOE": [GPIO() for _ in range(_PINS_IN_MCU_GPIOE)],
                "GPIOF": [GPIO() for _ in range(_PINS_IN_MCU_GPIOF)],
                "GPIOG": [GPIO() for _ in range(_PINS_IN_MCU_GPIOG)],
                "GPIOH": [GPIO() for _ in range(_PINS_IN_MCU_GPIOH)],
                "GPIOI": [GPIO() for _ in range(_PINS_IN_MCU_GPIOI)],
            },
            "I2C0_Expander0": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C0_Expander1": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C0_Expander2": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C0_Expander3": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C1_Expander0": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C1_Expander1": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C1_Expander2": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
            "I2C1_Expander3": {
                "P0": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P1": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
                "P2": [GPIO() for _ in range(_PINS_IN_EXPANDER_PORTS)],
            },
        })


nodesStatuses: list[NodeDeviceStatus] = []


def _getMCUPortPinIdxsFromPortPinStr(portpin: str) -> tuple:
    """
    Take string representing MCU IO and turn it into tuple of
    indices for lists in NodeDeviceStatus objects.
    Example: "B_1" -> (0, 0), "E_5" -> (2, 4) etc.
    """
    port = "GPIO" + portpin[0]
    pin = int(portpin[-1]) - 1

    return (port, pin)


def _getExpanderPortPinIdxsFromPortPinStr(portpin: str) -> tuple:
    """
    Take string representing MCU IO and turn it into tuple of
    indices for lists in NodeDeviceStatus objects.
    Example: "P0_2" -> (0, 1), "P2_4" -> (2, 3) etc.
    """
    return (portpin[:2], int(portpin[3])-1)


def generateListOfNodes(ioList: list[ios.InputOutput]):
    """ Parse list of IOs and generate list of CAN Nodes needed """

    tmp = [NodeDeviceStatus(address=i+1) for i in range(_MAX_NUMBER_OF_NODES)]

    for io in ioList:
        dir = IO_CONF.UNUSED
        if isinstance(io, ios.Output):
            dir = IO_CONF.OUTPUT
        elif isinstance(io, ios.Input):
            dir = IO_CONF.INPUT

        if io.device == "MCU":
            port, pin = _getMCUPortPinIdxsFromPortPinStr(io.portpin)
        else:
            port, pin = _getExpanderPortPinIdxsFromPortPinStr(io.portpin)

        tmp[int(io.nodeAddr)-1].statuses[io.device] = DEVICE_STATUS.UNKNOWN
        tmp[int(io.nodeAddr)-1].GPIOs[io.device][port][pin] = \
            GPIO(dir, IO_STATE.LOW)

    # Remove nodes with no IOs attached
    nodesStatuses.clear()
    for node in tmp:
        if node.statuses["MCU"] != DEVICE_STATUS.UNUSED:
            nodesStatuses.append(node)

    logging.debug(f"Generated List Of Nodes:\n{nodesStatuses}")


def updateNodeDeviceStatuses(nodeAddress: int, nodeRawData: list[int]):
    """
    Take raw data from CAN frame and address of a node and update adequate
    node status
    """
    global nodesStatuses
    nodeDefined = False

    for node in nodesStatuses:
        if node.address == nodeAddress:
            nodeDefined = True
            if nodeRawData[-1] & 0x01:
                node.statuses[devices[0]] = DEVICE_STATUS.ERROR
            else:
                node.statuses[devices[0]] = DEVICE_STATUS.OK

            # reversed so least significant byte is handled first
            for i, data in enumerate(reversed(nodeRawData[:-1])):
                for j in range(4):
                    if node.statuses[devices[i*4 + j]] != \
                            IO_CONF.UNUSED:
                        node.statuses[devices[i*4 + j]] = \
                            (data & (1 << j)) >> j

    if nodeDefined is False:
        logging.warning(
            "Odebrano dane od węzła, który nie posiada skonfigurowanych I/O"
        )


def updateNodeGpioStates(nodeAddress: int, nodeRawData: list[int]):
    pass


def generateStatusString(nodes: list[NodeDeviceStatus]) -> str:
    """
    Take list of node device statuses and generate status string to be
    displayed in the webpanel.
    """
    def statusToRepresentingCharacter(status: DEVICE_STATUS) -> str:
        """ Determine node status representing character for status string. """
        if status == DEVICE_STATUS.UNUSED:
            return ' _ '
        elif status == DEVICE_STATUS.UNKNOWN:
            return ' ? '
        elif status == DEVICE_STATUS.ERROR:
            return ' ! '
        elif status == DEVICE_STATUS.OK:
            return ' + '

    statusString: str = ""
    for node in nodes:
        statusString += str(node.address)
        statusString += ','
        statusString += statusToRepresentingCharacter(
                            node.statuses["MCU"]
                        )
        statusString += ','
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C0_Expander0"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C0_Expander1"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C0_Expander2"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C0_Expander3"]
                        )
        statusString += ','
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C1_Expander0"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C1_Expander1"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C1_Expander2"]
                        )
        statusString += statusToRepresentingCharacter(
                            node.statuses["I2C1_Expander3"]
                        )
        statusString += ','

    logging.debug(f"Status-string:\n{statusString}")

    return statusString[:-1]
